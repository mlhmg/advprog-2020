package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {

        public AgileAdventurer(Guild guild) {
                this.name = "Agile";
                //ToDo: Complete Me
                this.guild = guild;

        }

        @Override
        public void update() {
                String currQuest = this.guild.getQuestType();
                if (currQuest.equals("D") || currQuest.equals("R")) {
                        this.getQuests().add(this.guild.getQuest());
                }
        }
}
