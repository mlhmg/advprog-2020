package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithArmor implements DefenseBehavior {
//    return "ARMOR MAXIMUM"

    @Override
    public String getType() {
        return defend();
    }

    @Override
    public String defend() {
        return "INFINITI ARMORRRR";
    }
}
