package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithSword implements AttackBehavior {
//    return "Super Slash"

    @Override
    public String attack() {
        return "SLASHER";
    }

    @Override
    public String getType() {
        return attack();
    }
}
