package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithBarrier implements DefenseBehavior {
        //ToDo: Complete me
//    return "BENTENG TAKESHI"

    @Override
    public String getType() {
        return defend();
    }

    @Override
    public String defend() {
        return "BARRIER ON";
    }
}
