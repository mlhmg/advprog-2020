package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {

        public MysticAdventurer(Guild guild) {
                this.name = "Mystic";
                this.guild = guild;
                //ToDo: Complete Me
        }

        @Override
        public void update() {
                String currQuest = this.guild.getQuestType();
                if (currQuest.equals("D") || currQuest.equals("E")){
                        this.getQuests().add(this.guild.getQuest());
                }
        }

        //ToDo: Complete Me
}
